import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { UserService } from "../services/user.service";
import { IUser } from "../interfaces";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"],
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;

  constructor(private userService : UserService, private formBuilder: FormBuilder){}

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      login: ['', Validators.required],
      nom : ['', Validators.required], 
      prenom : ['', Validators.required], 
      password : ['', [Validators.required, Validators.minLength(6)]]
    });

  }

  get f() {
    return this.registerForm.controls;
  }

  submit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }


    else {
      let user= {
        username: this.registerForm.value.login,
        lastname: this.registerForm.value.nom,
        firstname: this.registerForm.value.prenom,
        password: this.registerForm.value.password
      } as IUser;
  
  
      this.userService.register(user); 
  
    }
    

  }
}
