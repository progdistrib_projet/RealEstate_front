// Imports
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MatButtonModule } from "@angular/material/button";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatCardModule } from "@angular/material/card";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { HttpClientModule } from "@angular/common/http";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatExpansionModule } from "@angular/material/expansion";
import {MatTabsModule} from '@angular/material/tabs'; 
import {MatRadioModule} from '@angular/material/radio'; 
import {MatDialogModule} from '@angular/material/dialog';


// Components
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { FourOhFourComponent } from "./four-oh-four/four-oh-four.component";
import { RegisterComponent } from "./register/register.component";
import { DelDialogComponent } from './deldialog/deldialog.component';

//Services
import { UserService } from "./services/user.service";
import { PropertyService } from "./services/property.service";
import { AccueilComponent } from "./accueil/accueil.component";
import { AuthGuard } from "./services/auth-guard.service";
import { RentDialogComponent } from './rent-dialog/rent-dialog.component';
import { EditDialogComponent } from './edit-dialog/edit-dialog.component';

// Routage
const appRoutes: Routes = [
  { path: "", component: LoginComponent },
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent },
  { path: "not-found", component: FourOhFourComponent },
  { path: "accueil", canActivate: [AuthGuard], component: AccueilComponent },
  { path: "**", redirectTo: "not-found" },
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    FourOhFourComponent,
    AccueilComponent,
    DelDialogComponent,
    RentDialogComponent,
    EditDialogComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    NoopAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatToolbarModule,
    MatExpansionModule,
    MatTabsModule,
    MatRadioModule,
    MatDialogModule
    
    ],
  providers: [UserService, AuthGuard, PropertyService],
  bootstrap: [AppComponent],
})
export class AppModule {}
