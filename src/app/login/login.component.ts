import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { UserService } from "../services/user.service";
import { ILogin, IUser } from "../interfaces";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  constructor(private userService: UserService) {}

  submit(f: NgForm) {
    let user = {
      login: f.value.login,
      password: f.value.password,
    } as ILogin;

    this.userService.login(user);
  }

  ngOnInit(): void {}
}
