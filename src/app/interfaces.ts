export interface IUser {
    username: string;
    lastname: string;
    firstname: string;
    password: string;
}

export interface ILogin {
    login: string;
    password: string;
}

export interface IProperty {
    idProperty: number,
    name : string;
    password: string;
    address : string;
    rentPrice: number;
    available: boolean;
    owner: IUser;
    tenant: IUser;
    floor: number;
    garden: boolean;
    garage: boolean;
    balcony: boolean;
    parking: boolean;
}