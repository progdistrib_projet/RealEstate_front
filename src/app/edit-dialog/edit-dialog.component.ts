import { Component, Inject, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { PropertyService } from "../services/property.service";

export interface DialogData {
  id: number;
  name: string;
  price: number;
  isAvailable: boolean;
}

@Component({
  selector: "app-edit-dialog",
  templateUrl: "./edit-dialog.component.html",
  styleUrls: ["./edit-dialog.component.css"],
})
export class EditDialogComponent implements OnInit {
  editForm: FormGroup;
  submitted = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private formBuilder: FormBuilder,
    private propertyService: PropertyService
  ) {}

  ngOnInit(): void {
    this.editForm = this.formBuilder.group({
      name: [this.data.name, Validators.required],
      price: [this.data.price, Validators.required],
      isAvailable: [this.data.isAvailable, Validators.required],
    });
  }

  submit() {
    this.submitted = true;

    if (this.editForm.invalid) {
      return;
    } else {
      this.propertyService.editProperty(
        this.data.id,
        this.editForm.value.name,
        this.editForm.value.price,
        this.editForm.value.isAvailable
      );
    }
  }
}
