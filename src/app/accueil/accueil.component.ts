import { Component, OnDestroy, OnInit } from "@angular/core";
import { GlobalConstants } from "../global-constants";
import { IProperty } from "../interfaces";
import { PropertyService } from "../services/property.service";
import { MatDialog } from "@angular/material/dialog";
import { DelDialogComponent } from "../deldialog/deldialog.component";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs";
import { ChangeDetectorRef } from "@angular/core";
import { RentDialogComponent } from "../rent-dialog/rent-dialog.component";
import { EditDialogComponent } from "../edit-dialog/edit-dialog.component";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: "app-accueil",
  templateUrl: "./accueil.component.html",
  styleUrls: ["./accueil.component.css"],
})
export class AccueilComponent implements OnInit {
  login: string;
  idUser: number;
  isAuthenticated: boolean;
  type: string;
  availableProperties: IProperty[];
  myProperties: IProperty[];
  propertyRented: IProperty;

  propertyForm: FormGroup;
  submitted = false;

  constructor(
    private propertyService: PropertyService,
    public dialog: MatDialog,
    private ref: ChangeDetectorRef,
    private router: Router,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.login = GlobalConstants.login;
    this.isAuthenticated = GlobalConstants.isAuthenticated;
    this.idUser = GlobalConstants.idUser;
    this.propertyForm = this.formBuilder.group({
      name: ['', Validators.required],
      address : ['', Validators.required], 
      rentPrice : ['100', [Validators.required, Validators.min(100)]],
      floor : ['1', [Validators.required, Validators.min(0), Validators.max(10)]],
      garden : [''],
      garage : [''],
      balcony : [''],
      parking : ['']
    });

  
    this.type = "house";

    this.fetchData();
  }


  get f() {
    return this.propertyForm.controls;
  }

  fetchData() {

    this.propertyService
      .getPropertiesAvailableByUser(this.idUser)
      .subscribe((resultat) => {
        this.availableProperties = resultat;
      });

    this.propertyService
      .getPropertiesByUser(this.idUser)
      .subscribe((resultat) => {
        this.myProperties = resultat;
      });

      this.propertyService.getPropertyByTenant(this.idUser).subscribe((resultat) => {
        this.propertyRented = resultat;
      });
  }

  submit() {
    this.submitted = true;

    if (this.propertyForm.invalid) {
      return;
    }

    if (this.type == "house") {
      let house = {
        name: this.propertyForm.value.name,
        address: this.propertyForm.value.address,
        rentPrice: this.propertyForm.value.rentPrice,
        available: true,
        floor: this.propertyForm.value.floor,
        garden: this.propertyForm.value.garden,
        garage: this.propertyForm.value.garage,
      };


      this.propertyService.addHouse(house);
    } else {
      let appartment = {
        name: this.propertyForm.value.name,
        address: this.propertyForm.value.address,
        rentPrice: this.propertyForm.value.rentPrice,
        available: true,
        balcony: this.propertyForm.value.balcony,
        parking: this.propertyForm.value.parking,
      };

      this.propertyService.addAppartment(appartment);
    }
  }

  alertDelete(name: string, id: number) {
    const dialogRef = this.dialog.open(DelDialogComponent, {
      data: {
        name: name,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.propertyService.deleteProperty(id);
      }
    });
  }

  alertRent(name: string, id: number) {
    const dialogRef = this.dialog.open(RentDialogComponent, {
      data: {
        name: name,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.propertyService.rentProperty(id);
      }
    });
  }

  alertRentBack(name: string, id: number) {
    const dialogRef = this.dialog.open(RentDialogComponent, {
      data: {
        name: name,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.propertyService.rentBackProperty(id);
      }
    });
  }

  editProperty(id: number, name: string, price: number, available: boolean) {
    const dialogRef = this.dialog.open(EditDialogComponent, {
      data: {
        id: id,
        name: name,
        price: price,
        isAvailable: available
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        console.log(result);
      }
    });
  }
}
