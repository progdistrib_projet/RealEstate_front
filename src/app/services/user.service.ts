import {
  throwError as observableThrowError,
  Observable,
  BehaviorSubject,
  Subject,
} from "rxjs";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { IUser, ILogin } from "../interfaces";
import { tap, catchError } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { GlobalConstants } from "../global-constants";

@Injectable()
export class UserService {
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private routeur: Router
  ) {}

  private status: number;

  register(user: IUser) {
    let toSend = {
      username: user.username,
      lastname: user.lastname,
      firstname: user.firstname,
      password: user.password,
    };

    this.http
      .post<any>(GlobalConstants.ipBack+"/users", toSend, { observe: "response" })
      .subscribe(
        (data) => {
          this.status = data.status;
          GlobalConstants.isAuthenticated = true;
          GlobalConstants.login = data.body.username;
          GlobalConstants.idUser = data.body.idUser;
          this.routeur.navigate(["/accueil"]);
        },
        (err) => {
          GlobalConstants.isAuthenticated = false;
          alert("L'inscription a échoué. Veuillez réessayez");
        }
      );
  }

  login(login: ILogin) {
    let toSend = {
      username: login.login,
      password: login.password,
    };

    this.http
      .post<any>(GlobalConstants.ipBack+"/login", toSend, { observe: "response" })
      .subscribe(
        (data) => {
          this.status = data.status;

          GlobalConstants.isAuthenticated = true;
          GlobalConstants.login = data.body.username;
          GlobalConstants.idUser = data.body.idUser;
          this.routeur.navigate(["/accueil"]);
        },
        (err) => {
          GlobalConstants.isAuthenticated = false;
          alert("L'authentification a échoué. Veuillez réessayez");
          console.log(err.message);
        }
      );
  }
}
