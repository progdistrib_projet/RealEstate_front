import { throwError as observableThrowError, Observable } from "rxjs";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { IUser, ILogin, IProperty } from "../interfaces";
import { tap, catchError } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { GlobalConstants } from "../global-constants";
import { ActivatedRoute, Router } from "@angular/router";
import { logging } from "protractor";

@Injectable()
export class PropertyService {
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private routeur: Router
  ) {}

  private status: number;


  getPropertiesAvailableByUser(idUser: number): Observable<IProperty[]> {
    return this.http.get<IProperty[]>(
    GlobalConstants.ipBack+"/properties/available/" + idUser
    );
  }

  getPropertiesByUser(idUser: number): Observable<IProperty[]> {
    return this.http.get<IProperty[]>(
      GlobalConstants.ipBack+"/properties/user/" + idUser
    );
  }

  getPropertyByTenant(idUser: number): Observable<IProperty> {
  
    return this.http.get<IProperty>(
      GlobalConstants.ipBack+"/property/rent/" + idUser
    );
  }

  deleteProperty(idProperty: number) {
    this.http
      .delete(GlobalConstants.ipBack+"/property/" + idProperty)
      .subscribe(() => {
        this.routeur
          .navigateByUrl("/", { skipLocationChange: true })
          .then(() => {
            this.routeur.navigate(["accueil"]);
          });
      });
  }

  addHouse(house: any) {
    let owner = {
      username: GlobalConstants.login,
    };
    let toSend = {
      name: house.name,
      address: house.address,
      rentPrice: house.rentPrice,
      available: house.available,
      floor: house.floor,
      garden: house.garden,
      garage: house.garage,
      owner: owner,
    };

    this.http
      .post<any>(GlobalConstants.ipBack+"/house", toSend, { observe: "response" })
      .subscribe(
        (data) => {
          this.status = data.status;
          this.routeur
            .navigateByUrl("/", { skipLocationChange: true })
            .then(() => {
              this.routeur.navigate(["accueil"]);
            });
        },
        (err) => {}
      );
  }

  addAppartment(appartment: any) {
    let owner = {
      username: GlobalConstants.login,
    };
    let toSend = {
      name: appartment.name,
      address: appartment.address,
      rentPrice: appartment.rentPrice,
      available: appartment.available,
      balcony: appartment.balcony,
      parking: appartment.parking,
      owner: owner,
    };

    this.http
      .post<any>(GlobalConstants.ipBack+"/appartment", toSend, {
        observe: "response",
      })
      .subscribe(
        (data) => {
          this.status = data.status;
          this.routeur
            .navigateByUrl("/", { skipLocationChange: true })
            .then(() => {
              this.routeur.navigate(["accueil"]);
            });
        },
        (err) => {
          console.log(err.message);
        }
      );
  }

  rentProperty(idProperty: number) {
    let tenant = {
      username: GlobalConstants.login,
    };

    let toSend = {
      tenant: tenant,
      available: false
    };

    this.http.put<any>(GlobalConstants.ipBack+"/property/"+idProperty, toSend, {
      observe: "response",
    })
    .subscribe(
      (data) => {
        this.status = data.status;
        this.routeur
          .navigateByUrl("/", { skipLocationChange: true })
          .then(() => {
            this.routeur.navigate(["accueil"]);
          });
      },
      (err) => {
        console.log(err.message);
      }
    );
  }

  rentBackProperty(idProperty: number) {

    let toSend = {
      tenant: null,
      available: true
    };

    this.http.put<any>(GlobalConstants.ipBack+"/property/"+idProperty, toSend, {
      observe: "response",
    })
    .subscribe(
      (data) => {
        this.status = data.status;
        this.routeur
          .navigateByUrl("/", { skipLocationChange: true })
          .then(() => {
            this.routeur.navigate(["accueil"]);
          });
      },
      (err) => {
        console.log(err.message);
      }
    );
  }

  editProperty(idProperty: number, name: string, price: number, isAvailable: boolean) {
    let toSend = {
      name: name,
      rentPrice: price,
      available: isAvailable
    };
  
    this.http.put<any>(GlobalConstants.ipBack+"/property/"+idProperty, toSend, {
      observe: "response",
    })
    .subscribe(
      (data) => {
        this.status = data.status;
        this.routeur
          .navigateByUrl("/", { skipLocationChange: true })
          .then(() => {
            this.routeur.navigate(["accueil"]);
          });
      },
      (err) => {
        console.log(err.message);
      }
    );
  }
  
}


