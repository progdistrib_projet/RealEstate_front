import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from "rxjs";
import { UserService } from './user.service';
import { Injectable } from '@angular/core';
import { GlobalConstants } from '../global-constants';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private userService: UserService, private router: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if(GlobalConstants.isAuthenticated) {
        return true;
    } else {
        this.router.navigate(['/register']);
    }
  }
}