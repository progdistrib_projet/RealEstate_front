export class GlobalConstants {
    public static isAuthenticated: boolean;
    public static login: string;
    public static idUser : number;
    public static emptyProperty: boolean;
    public static ipBack = "http://34.76.77.71:8080";
}