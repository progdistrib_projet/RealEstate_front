FROM node:alpine AS builder

WORKDIR /realestate_front/app

COPY ./realestate_front/ .

RUN npm install && \
    npm run build


FROM nginx:alpine

COPY --from=builder   ./realestate_front/app/dist/* /usr/share/nginx/html/


# # Create image based on the official Node 6 image from dockerhub
# FROM node:alpine

# # Create a directory where our app will be placed
# RUN mkdir -p /usr/src/app

# # Change directory so that our commands run inside this new directory
# WORKDIR /usr/src/app

# # Copy dependency definitions
# COPY ./realestate_front/package.json /usr/src/app

# # Install dependecies
# RUN npm install

# # Get all the code needed to run the app
# COPY . /usr/src/app

# # Expose the port the app runs in
# EXPOSE 4200

# # Serve the app
# CMD ["ng", "serve"]